import React, { Component } from 'react'
import model from './glassesImage/model.jpg'
import './glasses.css'

export default class Model extends Component {
  render() {
    let { name, desc, price, url } = this.props.glassPos;
    return (
      <div className="model">
        <div className="  card text-left">
          <img className="position-relative glassesModel" src={model} alt />
          <img className="position-absolute glasses" src={url} />
          <div className=" bg-dark text-light card-body pt-5">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{desc}</p>
            <h3>{price} $</h3>
          </div>
        </div>
      </div>
    )
  }
}
