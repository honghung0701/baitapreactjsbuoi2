import React, { Component } from 'react'

export default class ItemGlasses extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className='col-4 item text-left pt-3 mb-3'
      onClick={() => {
        this.props.changeGlasses(this.props.data);
      }}
      >
        <img src={url} alt="" />
      </div>
    )
  }
}
