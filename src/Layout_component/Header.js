import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <header>
        <div className='shadow-lg p-3 mb-5 bg-dark text-center'>
          <h2>TRY GLASSES APP ONLINE</h2>
        </div>
      </header>
    )
  }
}
