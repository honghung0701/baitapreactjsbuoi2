import React, { Component } from 'react'
import {dataGlasses} from './dataGlasses'
import ItemGlasses from './ItemGlasses'
import Model from './Model'
import './glasses.css'

export default class Glasses_layout extends Component {
  state = {
    glassesArr: dataGlasses,
    detail: dataGlasses,
  }
  renderGlassesList = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
        changeGlasses={this.handleChangeGlasses}
        data={item}
        key={index}
        />
      )
    })
  }
  handleChangeGlasses = (glass) => {
    this.setState({
      detail: glass,
    })
  }
  render() {
    return (
      <div>
        <div className="container d-flex col-12">
          <div className='col-4'>
            <Model glassPos={this.state.detail} />
          </div>
          <div className=" col-8 row">
            {this.renderGlassesList()}
          </div>
        </div>
      </div>
    )
  }
}
