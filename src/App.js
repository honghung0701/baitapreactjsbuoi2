import logo from './logo.svg';
import './App.css';
import Header from './Layout_component/Header';
import Glasses_layout from './Layout_component/Glasses_layout';

function App() {
  return (
    <div className="App">
      <Header />
      <Glasses_layout />
    </div>
  );
}

export default App;
